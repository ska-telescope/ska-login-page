Changelog
==========

1.0.0
=====

Release for general usage

0.9.0
*****

Initial conversion into a library containing components and functions for use with AAA.

NAL-1278

[Removed] Removed unused env variables.
[Changed] Disable MS Entra auth if not configured.
[Changed] Integration with the AAA components in the SKAO GUI Components Library.
[Changed] Added account profile picture to the login button.

NAL-1184

[Changed] Change the way that user profile details are loaded from MS Entra. It now loads from the Msal Context values instead of MS Graph via the Redux store.

BREAKING [Removed] Removed mocked authentication. Users are now required to use MS Entra for authentication.
[Changed] Added the env_scripts and updated the use of environment variables so that they can be updated according the requirements of application deployments.
[Changed] Moved the mocked roles out of the MS Entra Auth path into a separate button on the Header.




v0.4.0


NAL-1045

[Changed] Added the SKAO Shell to have an example integration of the authentication into a project that can be used as reference for other projects.




v0.3.0


NAL-861


BREAKING [Change] Removed OAuth-proxy and added integration with MS Entra with MS Graph to authenticate users and obtain user profile information.



NAL-962


BREAKING [Change] Changed the Webpack5 remote modules that are exposed to the following modules: MockAuthDialogs, SwitchAuthButton, SignInButton and SignOutButton.

BREAKING [Change] Created a new AuthProvider that includes the MsalProvider.

BREAKING [Change] Moved the dialog states to the ska-gui-local-storage.



NAL-961

[Changed] Moved the Kubernetes service and config maps of the deployments into the deployment template. Making it cleaner and easier to include in other charts.




v0.2.0


NAL-808

[Change] Fix ska-login-page docs link/build



NAL-874

[Change] Disable password field



STAR-79

[Change] Template as per React Skeleton



NAL-781

[Change] Extend authentication component to store authentication details in Redis




v0.1.0

[Added] Initial release

