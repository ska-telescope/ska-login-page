/**
 * Authentication Configuration for MSAL (Microsoft Authentication Library)
 *
 * This file defines the configuration settings for MSAL, which is used to authenticate
 * users with Azure Active Directory in a React TypeScript application.
 * Sensitive data such as client ID and tenant ID are loaded from environment variables
 * to enhance security.
 */
import { Configuration } from '@azure/msal-browser';
/**
 * MSAL Configuration Object
 *
 * This object contains authentication parameters and cache configuration for MSAL.
 * - `auth`: Contains authentication parameters like client ID, authority, and redirect URI.
 * - `cache`: Configures how tokens are stored in the browser.
 */
export declare const msalConfig: Configuration;
/**
 * Scopes for Login Request
 *
 * Defines the permissions that the application requests from the user during authentication.
 * - 'User.Read' allows the app to read the signed-in user's profile.
 * Additional scopes can be added as needed.
 */
export declare const loginRequest: {
  scopes: string[];
};
