import { ButtonColorTypes } from '@ska-telescope/ska-gui-components';
export interface ButtonUserProps {
  ariaDescription?: string;
  color?: ButtonColorTypes;
  label?: string;
  onClick?: Function;
  photo?: undefined | null | string;
  toolTip?: string;
  showPhoto?: boolean;
  showUsername?: boolean;
}
export declare function ButtonUser({
  ariaDescription,
  color,
  label,
  onClick,
  photo,
  showPhoto,
  showUsername,
  toolTip,
}: ButtonUserProps): JSX.Element;
