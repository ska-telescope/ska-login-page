import React from 'react';
import {
  ButtonColorTypes,
  ButtonSizeTypes,
  ButtonVariantTypes,
} from '@ska-telescope/ska-gui-components';
export declare const SPACER_HEADER = 70;
export declare const SPACER_FOOTER = 0;
export type Children = JSX.Element | JSX.Element[] | null;
export type Help = {
  content: Object;
  component: Object;
  showHelp: Boolean;
};
export type Telescope = {
  code: string;
  name: string;
  location: string;
  position: {
    lat: number;
    lon: number;
  };
  image: string;
};
export type Storage = {
  help?: Help;
  helpToggle?: Function;
  telescope?: Telescope;
  themeMode: string;
  toggleTheme: Function;
  updateTelescope?: Function;
};
export type AuthWrapperProperties = {
  application?: string;
  buttonLoginColor?: ButtonColorTypes;
  buttonLoginDisabled?: boolean;
  buttonLoginLabel?: string;
  buttonLoginSize?: ButtonSizeTypes;
  buttonLoginToolTip?: string;
  buttonLoginVariant?: ButtonVariantTypes;
  buttonLogoutColor?: ButtonColorTypes;
  buttonLogoutDisabled?: boolean;
  buttonLogoutLabel?: string;
  buttonLogoutSize?: ButtonSizeTypes;
  buttonLogoutToolTip?: string;
  buttonLogoutVariant?: ButtonVariantTypes;
  buttonUserChildren?: Children;
  buttonUserChildrenLast?: boolean;
  buttonUserColor?: ButtonColorTypes;
  buttonUserShowPhoto?: boolean;
  buttonUserShowUsername?: boolean;
  buttonUserMenu?: boolean;
  buttonUserToolTip?: string;
  footerChildren?: Children;
  headerChildren?: Children;
  iconDocsToolTip?: string;
  iconDocsURL: string;
  iconSKAOToolTip?: string;
  iconThemeToolTip?: string;
  mainChildren?: Children;
  storageHelp?: Help;
  storageHelpToggle?: Function;
  storageTelescope?: Telescope;
  storageThemeMode: string;
  storageToggleTheme: Function;
  storageUpdateTelescope?: Function;
  version?: string;
  versionTooltip?: string;
};
export interface UserProps {
  open: boolean;
  toggleDrawer: Function;
  properties: AuthWrapperProperties;
}
export declare function TheFooter(properties: AuthWrapperProperties): React.JSX.Element;
export declare function AuthWrapper(properties: AuthWrapperProperties): React.JSX.Element;
export default AuthWrapper;
