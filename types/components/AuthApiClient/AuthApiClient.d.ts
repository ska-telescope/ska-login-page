/**
 * Custom hook to create an authenticated API client.
 *
 * Provides a function to make API calls with the access token included in the headers.
 *
 * @returns A function to make authenticated API calls.
 */
export declare const AuthApiClient: () => (url: string, options?: RequestInit) => Promise<Response>;
export default AuthApiClient;
