type BaseRole = 'SuperAdmin' | 'Edit' | 'Read' | 'Admin';
type ExtendedRole = string;
export declare const useUserRoles: () => {
  /** All roles from token */
  allRoles: string[];
  /** Fundamental application roles */
  baseRoles: BaseRole[];
  /** Extended/custom roles (location-based, etc) */
  extendedRoles: string[];
  /** Special super admin flag */
  isSuperAdmin: boolean;
  hasBaseRole: (role: BaseRole) => boolean;
  hasExtendedRole: (role: ExtendedRole) => boolean;
  hasAnyRole: (...roles: (BaseRole | ExtendedRole)[]) => boolean;
  hasAllRoles: (...roles: (BaseRole | ExtendedRole)[]) => boolean;
};
export {};
