/**
 * Acquires an access token silently, or prompts the user if interaction is required.
 *
 * @returns The access token.
 * @throws Error if token acquisition fails.
 */
export declare const GetToken: () => Promise<string>;
