import { ButtonColorTypes } from '@ska-telescope/ska-gui-components';
import { Children } from '../AuthWrapper/AuthWrapper';
export interface ButtonUserMenuProps {
  ariaDescription?: string;
  color?: ButtonColorTypes;
  children?: Children;
  label?: string;
  logoutStart?: boolean;
  onClick?: Function;
  photo?: undefined | null | string;
  toolTip?: string;
  showPhoto?: boolean;
  showUsername?: boolean;
}
export declare function ButtonUserMenu({
  ariaDescription,
  color,
  children,
  label,
  logoutStart,
  onClick,
  photo,
  showPhoto,
  showUsername,
  toolTip,
}: ButtonUserMenuProps): JSX.Element;
