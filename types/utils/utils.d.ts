/**
 * Scopes for Login Request
 *
 * Defines the permissions that the application requests from the user during authentication.
 * - 'User.Read' allows the app to read the signed-in user's profile.
 * Additional scopes can be added as needed.
 */
export declare const loginRequest: {
  scopes: string[];
};
