import { InteractionRequiredAuthError } from '@azure/msal-browser';
import { useMsal } from '@azure/msal-react';
import { loginRequest } from '../../utils/utils';

/**
 * Acquires an access token silently, or prompts the user if interaction is required.
 *
 * @returns The access token.
 * @throws Error if token acquisition fails.
 */
export const GetToken = async (): Promise<string> => {
  const { instance, accounts } = useMsal();
  if (accounts.length > 0) {
    try {
      const response = await instance.acquireTokenSilent({
        ...loginRequest,
        account: accounts[0],
      });
      return response.accessToken;
    } catch (error) {
      if (error instanceof InteractionRequiredAuthError) {
        // Redirect to login page for interactive login
        await instance.acquireTokenRedirect(loginRequest);
        return ''; // Return empty string after redirecting
      } else {
        console.error('Failed to acquire token silently:', error);
        throw error; // Throw error to be handled by the caller
      }
    }
  } else {
    const errorMessage = 'No accounts available to acquire token.';
    console.error(errorMessage);
    throw new Error(errorMessage);
  }
};
