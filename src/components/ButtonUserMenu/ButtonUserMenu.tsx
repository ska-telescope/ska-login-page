import React from 'react';
import { Avatar, Divider, IconButton, Menu, MenuItem, Tooltip, Typography } from '@mui/material';
import { Box } from '@mui/system';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { Button, ButtonColorTypes, ButtonVariantTypes } from '@ska-telescope/ska-gui-components';
import { ButtonLogout } from '../ButtonLogout/ButtonLogout';
import { Children } from '../AuthWrapper/AuthWrapper';

export interface ButtonUserMenuProps {
  ariaDescription?: string;
  color?: ButtonColorTypes;
  children?: Children;
  label?: string;
  logoutStart?: boolean;
  onClick?: Function;
  photo?: undefined | null | string;
  toolTip?: string;
  showPhoto?: boolean;
  showUsername?: boolean;
}

export function ButtonUserMenu({
  ariaDescription = 'User Button',
  color = ButtonColorTypes.Inherit,
  children,
  label = 'username',
  logoutStart = false,
  onClick,
  photo,
  showPhoto = false,
  showUsername = false,
  toolTip = 'Additional user functionality including sign out',
}: ButtonUserMenuProps): JSX.Element {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const openMenu = Boolean(anchorEl);

  const handleMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    if (onClick) {
      onClick();
    } else {
      setAnchorEl(event.currentTarget);
    }
  };

  const getInitials = (name: string) => {
    const initials = name
      .split(' ')
      .map((n) => n[0])
      .join('')
      .toUpperCase();
    return initials;
  };

  return (
    <>
      {showUsername && (
        <>
          <Button
            icon={
              showPhoto && photo ? (
                <img
                  src={photo}
                  alt="Profile"
                  style={{ borderRadius: '50%', width: '32px', height: '32px', objectFit: 'cover' }}
                />
              ) : (
                <AccountCircleIcon />
              )
            }
            aria-controls={openMenu ? 'user-menu' : undefined}
            aria-description={ariaDescription}
            aria-expanded={openMenu ? 'true' : undefined}
            aria-haspopup="true"
            aria-label={label}
            color={color}
            label={label}
            onClick={handleMenuOpen}
            testId="usernameMenu"
            toolTip={toolTip}
            variant={ButtonVariantTypes.Contained}
          />
          <Menu
            id="user-menu"
            anchorEl={anchorEl}
            open={openMenu}
            onClose={() => setAnchorEl(null)}
            MenuListProps={{
              'aria-labelledby': 'basic-button',
            }}
          >
            {logoutStart && children}
            <MenuItem onClick={() => setAnchorEl(null)}>
              {' '}
              <ButtonLogout color={ButtonColorTypes.Inherit} variant={ButtonVariantTypes.Text} />
            </MenuItem>
            {!logoutStart && children}
          </Menu>
        </>
      )}
      {!showUsername && (
        <>
          <Tooltip data-testid="usernameIcon" title={toolTip} arrow>
            <span>
              <IconButton
                aria-controls={openMenu ? 'user-menu' : undefined}
                aria-description={ariaDescription}
                aria-expanded={openMenu ? 'true' : undefined}
                aria-haspopup="true"
                aria-label={toolTip}
                onClick={onClick ? onClick() : null}
                style={{ cursor: 'pointer' }}
                data-testid="usernameMenu"
              >
                {showPhoto && photo && (
                  <Box
                    sx={{
                      border: 3,
                      borderColor: color + '.main',
                      borderRadius: '50%',
                      height: 42,
                      width: 44,
                    }}
                  >
                    <img
                      src={photo}
                      alt="Profile"
                      style={{ paddingTop: 3, borderRadius: '50%', width: '30px' }}
                    />
                  </Box>
                )}
                {!(showPhoto && photo) && (
                  <Avatar
                    sx={{
                      backgroundColor: color + '.main',
                    }}
                  >
                    <Typography variant="body1">{getInitials(label)}</Typography>
                  </Avatar>
                )}
              </IconButton>
            </span>
          </Tooltip>
          <Menu
            id="user-menu"
            anchorEl={anchorEl}
            open={openMenu}
            onClose={() => setAnchorEl(null)}
            MenuListProps={{
              'aria-labelledby': 'basic-button',
            }}
          >
            {logoutStart && children}
            {logoutStart && children && <Divider component="li" />}
            <MenuItem onClick={() => setAnchorEl(null)}>
              <ButtonLogout color={ButtonColorTypes.Inherit} variant={ButtonVariantTypes.Text} />
            </MenuItem>
            {!logoutStart && children && <Divider component="li" />}
            {!logoutStart && children}
          </Menu>
        </>
      )}
    </>
  );
}
