import { ButtonUserMenu } from '../ButtonUserMenu/ButtonUserMenu';

export default {
  title: 'Example/ButtonUserMenu',
  component: ButtonUserMenu,
  parameters: {
    layout: 'centered',
  },
};

export const Default = {
  args: {
    toolTip: 'Button User Menu Tooltip',
    showPhoto: false,
    showUsername: true,
  },
};
