import { ButtonColorTypes, ButtonSizeTypes } from '@ska-telescope/ska-gui-components';
import { AuthWrapper } from '../AuthWrapper/AuthWrapper';

export default {
  title: 'Example/AuthWrapper',
  component: AuthWrapper,
  parameters: {
    layout: 'centered',
  },
};

export const Default = {
  args: {
    mainChildren: 'MAIN CHILD',
    footerChildren: 'FOOTER CHILD',
    application: 'Application',
    buttonLoginColor: ButtonColorTypes.Success,
    buttonLoginDisabled: false,
    buttonLoginLabel: 'BUTTON LOGIN',
    buttonLoginToolTip: 'BUTTON LOGIN TOOLTIP',
    buttonLogoutColor: ButtonColorTypes.Secondary,
    buttonLogoutDisabled: false,
    buttonLogoutLabel: 'BUTTON LOGOUT',
    buttonLogoutToolTip: 'BUTTON LOGOUT TOOLTIP',
    buttonUserColor: ButtonColorTypes.Inherit,
    buttonUserShowPhoto: false,
    buttonUserShowUsername: true,
    buttonUserToolTip: 'BUTTON USER TOOLTIP',
    headerChildren: 'HEADER CHILD',
    docsIconToolTip: 'DOCS ICON TOOLTIP',
    docsURL: 'DOCS URL',
    skaoLogoToolTip: 'SKAO LOGO TOOLTIP',
    themeModeToolTip: 'THEME MODE TOOLTIP',
    storageThemeMode: 'dark',
    storageToggleTheme: null,
    version: '1.2.3',
    versionTooltip: '6.6.6',
  },
};
