import React from 'react';
import { useMsal, MsalAuthenticationTemplate } from '@azure/msal-react';
import {
  ButtonColorTypes,
  ButtonSizeTypes,
  ButtonVariantTypes,
  CopyrightModal,
  Footer,
  Header,
  SPACER_VERTICAL,
  Spacer,
} from '@ska-telescope/ska-gui-components';
import { InteractionType } from '@azure/msal-browser';
import { Box, Divider, Drawer, Grid2 as Grid, Paper, Stack } from '@mui/material';
import { InteractionRequiredAuthError, InteractionStatus } from '@azure/msal-browser';
import { ButtonLogin } from '../ButtonLogin/ButtonLogin';
import { ButtonLogout } from '../ButtonLogout/ButtonLogout';
import { ButtonUser } from '../ButtonUser/ButtonUser';
import { ButtonUserMenu } from '../ButtonUserMenu/ButtonUserMenu';

export const SPACER_HEADER = 70;
export const SPACER_FOOTER = 0;

export type Children = JSX.Element | JSX.Element[] | null;

export type Help = {
  content: Object;
  component: Object;
  showHelp: Boolean;
};

export type Telescope = {
  code: string;
  name: string;
  location: string;
  position: {
    lat: number;
    lon: number;
  };
  image: string;
};

export type Storage = {
  help?: Help;
  helpToggle?: Function;
  telescope?: Telescope;
  themeMode: string;
  toggleTheme: Function;
  updateTelescope?: Function;
};

export type AuthWrapperProperties = {
  application?: string;
  buttonLoginColor?: ButtonColorTypes;
  buttonLoginDisabled?: boolean;
  buttonLoginLabel?: string;
  buttonLoginSize?: ButtonSizeTypes;
  buttonLoginToolTip?: string;
  buttonLoginVariant?: ButtonVariantTypes;
  buttonLogoutColor?: ButtonColorTypes;
  buttonLogoutDisabled?: boolean;
  buttonLogoutLabel?: string;
  buttonLogoutSize?: ButtonSizeTypes;
  buttonLogoutToolTip?: string;
  buttonLogoutVariant?: ButtonVariantTypes;
  buttonUserChildren?: Children;
  buttonUserChildrenLast?: boolean;
  buttonUserColor?: ButtonColorTypes;
  buttonUserShowPhoto?: boolean;
  buttonUserShowUsername?: boolean;
  buttonUserMenu?: boolean;
  buttonUserToolTip?: string;
  footerChildren?: Children;
  headerChildren?: Children;
  iconDocsToolTip?: string;
  iconDocsURL: string;
  iconSKAOToolTip?: string;
  iconThemeToolTip?: string;
  mainChildren?: Children;
  storageHelp?: Help;
  storageHelpToggle?: Function;
  storageTelescope?: Telescope;
  storageThemeMode: string;
  storageToggleTheme: Function;
  storageUpdateTelescope?: Function;
  version?: string;
  versionTooltip?: string;
};

export interface UserProps {
  open: boolean;
  toggleDrawer: Function;
  properties: AuthWrapperProperties;
}

const User = ({ open, toggleDrawer, properties }: UserProps) => {
  return (
    <div>
      <Drawer anchor="right" open={open} onClose={toggleDrawer(false)}>
        <Box m={1} sx={{ minWidth: '15vw' }}>
          <Stack sx={{ height: '95%' }} spacing={2}>
            {properties.buttonUserChildrenLast && properties.buttonUserChildren}
            <ButtonLogout
              color={properties.buttonLogoutColor}
              disabled={properties.buttonLogoutDisabled}
              label={properties.buttonLogoutLabel}
              size={properties.buttonLogoutSize}
              toolTip={properties.buttonLogoutToolTip}
              variant={properties.buttonLogoutVariant}
            />
            {!properties.buttonUserChildrenLast && properties.buttonUserChildren}
          </Stack>
        </Box>
      </Drawer>
    </div>
  );
};

async function getMsEntraProfilePicture(accessToken: string) {
  const headers = new Headers();
  if (accessToken) {
    try {
      const bearer = `Bearer ${accessToken}`;
      headers.append('Authorization', bearer);

      const options = {
        method: 'GET',
        headers,
      };

      const stream = await fetch(
        'https://graph.microsoft.com/v1.0/me/photos/48x48/$value',
        options,
      );
      const blob = await stream.blob();
      const url = URL.createObjectURL(blob);
      return url;
    } catch (error) {
      console.error('Error fetching MS Graph data:', error);
      return null;
    }
  }
  return null;
}

function TheHeader(
  properties: AuthWrapperProperties,
  setOpenUser: {
    (newOpen: boolean): () => void;
    (arg0: boolean): React.MouseEventHandler<HTMLButtonElement> | undefined;
  },
): React.JSX.Element {
  const { instance, inProgress, accounts } = useMsal();
  const username = accounts.length > 0 ? accounts[0].name : '';
  const [photo, setPhoto] = React.useState<string | null | undefined>(null);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  function ProfileIcon() {
    React.useEffect(() => {
      if (!photo && inProgress === InteractionStatus.None) {
        const accessTokenRequest = {
          scopes: ['user.read'],
          account: accounts[0],
        };
        instance
          .acquireTokenSilent(accessTokenRequest)
          .then((accessTokenResponse) => {
            const { accessToken } = accessTokenResponse;
            getMsEntraProfilePicture(accessToken).then((response) => {
              setPhoto(response);
            });
          })
          .catch((error) => {
            if (error instanceof InteractionRequiredAuthError) {
              instance.acquireTokenPopup(accessTokenRequest).then((accessTokenResponse) => {
                const { accessToken } = accessTokenResponse;
                getMsEntraProfilePicture(accessToken).then((response) => {
                  setPhoto(response);
                });
              });
            }
          });
      }
    }, [instance, accounts, inProgress, photo]);
  }

  ProfileIcon();

  const signIn = (properties: AuthWrapperProperties) => (
    <>
      <MsalAuthenticationTemplate interactionType={InteractionType.None} />
      {username && properties.buttonUserMenu && (
        <ButtonUserMenu
          color={properties.buttonUserColor}
          children={properties.buttonUserChildren}
          label={username}
          logoutStart={!properties.buttonUserChildrenLast}
          photo={photo}
          showPhoto={properties.buttonUserShowPhoto}
          showUsername={properties.buttonUserShowUsername}
          toolTip={properties.buttonUserToolTip}
        />
      )}
      {username && !properties.buttonUserMenu && (
        <>
          <ButtonUser
            color={properties.buttonUserColor}
            label={username}
            onClick={() => setOpenUser(true)}
            photo={photo}
            showPhoto={properties.buttonUserShowPhoto}
            showUsername={properties.buttonUserShowUsername}
            toolTip={properties.buttonUserToolTip}
          />
        </>
      )}
      {!username && (
        <ButtonLogin
          color={properties.buttonLoginColor}
          disabled={properties.buttonLoginDisabled}
          label={properties.buttonLoginLabel}
          size={properties.buttonLoginSize}
          toolTip={properties.buttonLoginToolTip}
          variant={properties.buttonLoginVariant}
        />
      )}
    </>
  );

  return (
    <Header
      docs={{
        tooltip: properties.iconDocsToolTip
          ? properties.iconDocsToolTip
          : 'Click here for SKAO documentation',
        url: properties.iconDocsURL,
      }}
      title={properties.application ? properties.application : 'SKA APPLICATION'}
      testId="skaHeader"
      toolTip={{
        skao: properties.iconSKAOToolTip ? properties.iconSKAOToolTip : 'SKAO WebSite',
        mode: properties.iconThemeToolTip ? properties.iconThemeToolTip : 'Colour Mode Toggle',
      }}
      storage={{
        help: properties.storageHelp,
        helpToggle: properties.storageHelpToggle,
        telescope: properties.storageTelescope,
        themeMode: properties.storageThemeMode,
        toggleTheme: properties.storageToggleTheme,
        updateTelescope: properties.storageUpdateTelescope,
      }}
    >
      {signIn(properties)}
    </Header>
  );
}

export function TheFooter(properties: AuthWrapperProperties): React.JSX.Element {
  const [showCopyright, setShowCopyright] = React.useState(false);

  return (
    <>
      <CopyrightModal copyrightFunc={setShowCopyright} show={showCopyright} />
      <Footer
        copyrightFunc={setShowCopyright}
        testId="footerId"
        version={properties.version}
        versionTooltip={properties.versionTooltip}
      >
        {properties.footerChildren}
      </Footer>
    </>
  );
}

export function AuthWrapper(properties: AuthWrapperProperties) {
  const [openUser, setOpenUser] = React.useState(false);
  const { accounts } = useMsal();
  const username = accounts.length > 0 ? accounts[0].name : '';

  React.useEffect(() => {
    if (username === '') {
      setOpenUser(false);
    }
  }, [username]);

  const toggleDrawer = (newOpen: boolean) => () => {
    setOpenUser(newOpen);
  };

  return (
    <Paper elevation={0} sx={{ height: '100%', backgroundColor: 'primary.main' }}>
      {TheHeader(properties, toggleDrawer)}
      {!properties.buttonUserMenu && (
        <User open={openUser} toggleDrawer={toggleDrawer} properties={properties} />
      )}
      <Paper
        elevation={0}
        sx={{
          backgroundColor: 'primary.main',
          width: '100vw',
          minHeight: '100vh',
        }}
      >
        <div>
          <Spacer size={SPACER_HEADER} axis={SPACER_VERTICAL} />
          <main>{properties.mainChildren}</main>
          <Spacer size={SPACER_FOOTER} axis={SPACER_VERTICAL} />
        </div>
      </Paper>
      {TheFooter(properties)}
    </Paper>
  );
}

export default AuthWrapper;
