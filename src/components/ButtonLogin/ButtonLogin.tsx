import React from 'react';
import { useMsal } from '@azure/msal-react';
import {
  Button,
  ButtonColorTypes,
  ButtonSizeTypes,
  ButtonVariantTypes,
} from '@ska-telescope/ska-gui-components';
import { loginRequest } from '../../utils/utils';

export interface ButtonLoginProps {
  ariaDescription?: string;
  color?: ButtonColorTypes;
  component?: string;
  disabled?: boolean;
  icon?: JSX.Element | string;
  label?: string;
  onError?: Function;
  size?: ButtonSizeTypes;
  testId?: string;
  toolTip?: string;
  variant?: ButtonVariantTypes;
}

export function ButtonLogin({
  ariaDescription = 'Login Button',
  color = ButtonColorTypes.Secondary,
  component = 'button',
  disabled = false,
  icon = 'login',
  label = 'Sign In',
  onError,
  size = ButtonSizeTypes.Medium,
  testId = 'ButtonLogin-' + label,
  toolTip = 'Successful sign in will provide to access user specific features',
  variant = ButtonVariantTypes.Contained,
}: ButtonLoginProps): JSX.Element {
  const { instance } = useMsal();

  function handleLogin() {
    instance.loginRedirect(loginRequest).catch((e) => {
      onError ? onError(e) : null;
    });
  }

  return (
    <Button
      ariaDescription={ariaDescription}
      color={color}
      component={component}
      disabled={disabled}
      icon={icon}
      label={label}
      onClick={() => handleLogin()}
      size={size}
      testId={testId}
      toolTip={toolTip}
      variant={variant}
    />
  );
}
