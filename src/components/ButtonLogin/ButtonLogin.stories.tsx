import { ButtonColorTypes, ButtonVariantTypes } from '@ska-telescope/ska-gui-components';
import { ButtonLogin } from '../ButtonLogin/ButtonLogin';

export default {
  title: 'Example/ButtonLogin',
  component: ButtonLogin,
  parameters: {
    layout: 'centered',
  },
};

export const Default = {
  args: {
    ariaDescription: 'Login Button',
    color: ButtonColorTypes.Secondary,
    label: 'Sign in',
    toolTip: 'Successful sign in will provide to access user specific features',
    variant: ButtonVariantTypes.Contained,
  },
};
