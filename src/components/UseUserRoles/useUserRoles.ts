import { useMsal } from '@azure/msal-react';

type BaseRole = 'SuperAdmin' | 'Edit' | 'Read' | 'Admin';
type ExtendedRole = string; // Could be location-based or other future roles

export const useUserRoles = () => {
  const { accounts } = useMsal();

  // Get all roles from token
  const roles = accounts[0]?.idTokenClaims?.roles || [];
  const allRoles = Array.isArray(roles) ? roles : [roles];

  // Categorize roles
  const baseRoles = allRoles.filter((role) =>
    ['SuperAdmin', 'Edit', 'Read', 'Admin'].includes(role),
  ) as BaseRole[];

  const extendedRoles = allRoles.filter(
    (role) => !['SuperAdmin', 'Edit', 'Read', 'Admin'].includes(role),
  ) as ExtendedRole[];

  const isSuperAdmin = baseRoles.includes('SuperAdmin');

  return {
    /** All roles from token */
    allRoles,
    /** Fundamental application roles */
    baseRoles,
    /** Extended/custom roles (location-based, etc) */
    extendedRoles,
    /** Special super admin flag */
    isSuperAdmin,

    // Core checks
    hasBaseRole: (role: BaseRole) => baseRoles.includes(role),
    hasExtendedRole: (role: ExtendedRole) => extendedRoles.includes(role),

    // Flexible checks
    hasAnyRole: (...roles: (BaseRole | ExtendedRole)[]) => roles.some((r) => allRoles.includes(r)),

    hasAllRoles: (...roles: (BaseRole | ExtendedRole)[]) =>
      roles.every((r) => allRoles.includes(r)),
  };
};
