import { ButtonColorTypes, ButtonVariantTypes } from '@ska-telescope/ska-gui-components';
import { ButtonLogout } from '../ButtonLogout/ButtonLogout';

export default {
  title: 'Example/ButtonLogout',
  component: ButtonLogout,
  parameters: {
    layout: 'centered',
  },
};

export const Default = {
  args: {
    ariaDescription: 'aria Description',
    color: ButtonColorTypes.Secondary,
    icon: 'logout',
    label: 'Button Logout Label',
    toolTip: 'Button Logout Tooltip',
    variant: ButtonVariantTypes.Outlined,
  },
};
