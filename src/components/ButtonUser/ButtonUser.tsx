import React from 'react';
import { Avatar, IconButton, Tooltip, Typography } from '@mui/material';
import { Box } from '@mui/system';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { Button, ButtonColorTypes, ButtonVariantTypes } from '@ska-telescope/ska-gui-components';

export interface ButtonUserProps {
  ariaDescription?: string;
  color?: ButtonColorTypes;
  label?: string;
  onClick?: Function;
  photo?: undefined | null | string;
  toolTip?: string;
  showPhoto?: boolean;
  showUsername?: boolean;
}

export function ButtonUser({
  ariaDescription = 'User Button',
  color = ButtonColorTypes.Inherit,
  label = 'username',
  onClick,
  photo,
  showPhoto = false,
  showUsername = false,
  toolTip = 'Additional user functionality including sign out',
}: ButtonUserProps): JSX.Element {
  const getInitials = (name: string) => {
    const initials = name
      .split(' ')
      .map((n) => n[0])
      .join('')
      .toUpperCase();
    return initials;
  };

  return (
    <>
      {showUsername && (
        <Button
          icon={
            showPhoto && photo ? (
              <img
                src={photo}
                alt="Profile"
                style={{ borderRadius: '50%', width: '32px', height: '32px', objectFit: 'cover' }}
              />
            ) : (
              <AccountCircleIcon />
            )
          }
          aria-label={label}
          color={color}
          label={label}
          onClick={onClick ? onClick() : null}
          testId="userName"
          toolTip={toolTip}
          variant={ButtonVariantTypes.Contained}
        />
      )}
      {!showUsername && (
        <Tooltip data-testid="usernameIcon" title={toolTip} arrow>
          <span>
            <IconButton
              aria-label={toolTip}
              onClick={onClick ? onClick() : null}
              style={{ cursor: 'pointer' }}
            >
              {showPhoto && photo && (
                <Box
                  sx={{
                    border: 3,
                    borderColor: color + '.main',
                    borderRadius: '50%',
                    height: 42,
                    width: 44,
                  }}
                >
                  <img
                    src={photo}
                    alt="Profile"
                    style={{ paddingTop: 3, borderRadius: '50%', width: '30px' }}
                  />
                </Box>
              )}
              {!(showPhoto && photo) && (
                <Avatar
                  sx={{
                    backgroundColor: color + '.main',
                  }}
                >
                  <Typography variant="body1">{getInitials(label)}</Typography>
                </Avatar>
              )}
            </IconButton>
          </span>
        </Tooltip>
      )}
    </>
  );
}
