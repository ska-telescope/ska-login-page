import { ButtonColorTypes, ButtonVariantTypes } from '@ska-telescope/ska-gui-components';
import { ButtonUser } from '../ButtonUser/ButtonUser';

export default {
  title: 'Example/ButtonUser',
  component: ButtonUser,
  parameters: {
    layout: 'centered',
  },
};

export const Default = {
  args: {
    toolTip: 'Button User Tooltip',
    showPhoto: false,
    showUsername: true,
  },
};
