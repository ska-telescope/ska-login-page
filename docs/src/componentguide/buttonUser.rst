ButtonUser
~~~~~~~~~~~~

*Overview*

Button that once clicked will open a container to display user-focused options including Sign out
This can be used as a separate component, however it is also part of the AuthWrapper component

.. tip:: 

   - The component has been provided all the defaults needed to work within a standard SKA application.

.. figure:: /_static/img/components/buttonuser.png
   :width: 15%

.. code-block:: sh
   :caption: Example : Default usage

   import { ButtonUser } from '@ska-telescope/ska-login-page';

   ...

   <ButtonUser />


.. csv-table:: Properties
   :header: "Property", "Type", "Required", "Notes"

   "ariaDescription", "string", "No", "Used by Screen Readers"
   "color", "ButtonColorTypes", "No", "Default : ButtonColorTypes.Inherit"
   "label", "string", "No", "Default : username"
   "onClick", "Function", "No", "Determines action(s) to be taken when the button is clicked"
   "testId", "string", "No", "Fixed with a value of username. Identifier for testing purposes"
   "toolTip", "string", "No", "Default : Additional user functionality including sign out"
   "variant", "ButtonVariantTypes", "No", "Default : ButtonVariantTypes.Contained"
    
.. csv-table:: Constants
   :header: "Constant", "Possible Values", "Usage"

   "ButtonColorTypes", "Error,Inherit,Secondary,Success,Warning", "Button coloring"
   "ButtonSizeTypes", "Small, Medium, Large", "Button sizing"
   "ButtonVariantTypes", "Contained,Outlined,Text", "Button styling"

.. admonition:: Testing Identifier

   Provided by the property *testId*