useUserRoles
~~~~~~~~~~~~

*Overview*

Custom hook that provides a number of functions that allow for user role determination

.. note:: 

   - This has been taken 'as is' from the EMS portal, and so some functionality may need to be adjusted

.. code-block:: sh
   :caption: Example : Default usage

   import { useUserRoles } from '@ska-telescope/ska-login-page';

   ...

   const { isSuperAdmin } = useUserRoles();

.. csv-table:: Available functions
   :header: "Function", "Notes"

   "allRoles", "All roles associated to the token"
   "baseRoles", "Fundamental application roles only"
   "extendedRoles", "Extended/custom roles (location-based, etc) only"
   "isSuperAdmin", "Special super admin flag"
   "hasBaseRole: (role: BaseRole)", "return true if the provided base role is present"
   "hasExtendedRole: (role: ExtendedRole)", "return true if the provided extended role is present"
   "hasAnyRole: (...roles: (BaseRole | ExtendedRole)[])", "return true if any of the provided roles are present"
   "hasAllRoles: (...roles: (BaseRole | ExtendedRole)[])", "return true if all of the provided roles are present"