AuthProvider
~~~~~~~~~~~~

*Overview*

This is a component needs to wraps around all parts of the application that need authentication.
It needs to be provided with three properties

.. code-block:: sh
   :caption: Example : Default usage

   import { AuthProvider } from '@ska-telescope/ska-login-page';

   ...

   <AuthProvider
      MSENTRA_CLIENT_ID={MSENTRA_CLIENT_ID}
      MSENTRA_TENANT_ID={MSENTRA_TENANT_ID}
      MSENTRA_REDIRECT_URI={MSENTRA_REDIRECT_URI}
   >
        Code that requires authentication here
   </AuthProvider>

.. csv-table:: Properties
   :header: "Property", "Usage"

   "MSENTRA_CLIENT_ID", "Unique identifier assigned to the application by Microsoft Entra ID when it is registered"
   "MSENTRA_TENANT_ID", "Unique identifier associated to the SKA's instance of Microsoft Entra ID"
   "MSENTRA_REDIRECT_URI", "This is the location where the Microsoft Entra authentication server sends the user they have successfully authorized"
    
.. tip:: 

   - Ensure that multiple  redirect URI's have been registered with Microsoft Entra as this will vary between local and deployed instances