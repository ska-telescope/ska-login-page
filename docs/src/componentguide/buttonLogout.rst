ButtonLogout
~~~~~~~~~~~~

*Overview*

Button that once clicked will invoke the MSAL libraries to sign the user out.
This can be used as a separate component, however it is also part of the AuthWrapper component

.. tip:: 

   - The component has been provided all the defaults needed to work within a standard SKA application.

.. figure:: /_static/img/components/buttonlogout.png
   :width: 15%

.. code-block:: sh
   :caption: Example : Default usage

   import { ButtonLogout } from '@ska-telescope/ska-login-page';

   ...

   <ButtonLogout />


.. csv-table:: Properties
   :header: "Property", "Type", "Required", "notes"

   "ariaDescription", "string", "No", "Used by Screen Readers"
   "color", "ButtonColorTypes", "No", "Default : ButtonColorTypes.Inherit"
   "disabled", "boolean", "No", "Disabled when true, default set to false"
   "icon", "JSX.Element | string", "No", "Standard Logout Icon is displayed"
   "label", "string", "No", "Default : Sign out"
   "onError", "Function", "No", "Determines action(s) to be taken upon an error condition"
   "size", "ButtonSizeTypes", "No", "Default : ButtonSizeTypes.Medium"
   "testId", "string", "No", "Set to ButtonLogout-{{label}}.  Identifier for testing purposes"
   "toolTip", "string", "No", "Default : Sign out of SKA application(s)"
   "variant", "ButtonVariantTypes", "No", "Default : ButtonVariantTypes.Contained"

.. csv-table:: Constants
   :header: "Constant", "Possible Values", "Usage"

   "ButtonColorTypes", "Error,Inherit,Secondary,Success,Warning", "Button coloring"
   "ButtonSizeTypes", "Small, Medium, Large", "Button sizing"
   "ButtonVariantTypes", "Contained,Outlined,Text", "Button styling"

.. admonition:: Testing Identifier

   Provided by the property *testId*