ButtonLogin
~~~~~~~~~~~

*Overview*

Button that once clicked will invoke the MSAL libraries to sign the user in.
This can be used as a separate component, however it is also part of the AuthWrapper component

.. tip:: 

   - The component has been provided all the defaults needed to work within a standard SKA application.

.. figure:: /_static/img/components/buttonlogin.png
   :width: 15%

.. code-block:: sh
   :caption: Example : Default usage

   import { ButtonLogin } from '@ska-telescope/ska-login-page';

   ...

   <ButtonLogin />


.. csv-table:: Properties
   :header: "Property", "Type", "Required", "Notes"

   "ariaDescription", "string", "No", "Used by Screen Readers"
   "color", "ButtonColorTypes", "No", "Default : ButtonColorTypes.Secondary"
   "disabled", "boolean", "No", "Disabled when true, default is false"
   "icon", "JSX.Element | string", "No", "Standard Login Icon is displayed"
   "label", "string", "No", "Default : Sign in"
   "onError", "Function", "No", "Determines action(s) to be taken upon an error condition"
   "size", "ButtonSizeTypes", "No", "Default : ButtonSizeTypes.Medium"
   "testId", "string", "No", "Default : buttonLogin-{{label}}. Identifier for testing purposes"
   "toolTip", "string", "No", "Default : Successful sign in will provide to access user specific features"
   "variant", "ButtonVariantTypes", "No", "Default : ButtonVariantTypes.Contained"

.. csv-table:: Constants
   :header: "Constant", "Possible Values", "Usage"

   "ButtonColorTypes", "Error,Inherit,Secondary,Success,Warning", "Button coloring"
   "ButtonSizeTypes", "Small, Medium, Large", "Button sizing"
   "ButtonVariantTypes", "Contained,Outlined,Text", "Button styling"

.. admonition:: Testing Identifier

   Provided by the property *testId*