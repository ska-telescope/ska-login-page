Version
~~~~~~~

*Overview*

This is a simple constant that provides the latest version of the library

*Image is not applicable for this component*

.. admonition:: *Code snippet* ./services/theme.tsx

   import { SKA_GUI_AAA_VERSION } from '@ska-telescope/ska-login-page';
   
.. admonition:: Testing Identifier

   n/a