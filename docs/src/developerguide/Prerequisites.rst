Prerequisites
~~~~~~~~~~~~~

**Application registration**

To integrate MS Entra authentication into a SPA (Single Page Application) or WEB API, the application needs to be registered with MS Entra by the IT administrators. This registration can be done through the IT Support `Service Desk <https://jira.skatelescope.org/servicedesk/customer/portal/1/group/5>`_  and opening a request application registration for SSO ticket.

For this registration you will need:

- Your application name.
- All the redirect URL's that is expected for this application. This is the URL where your application will be hosted and it is used by MS Entra to redirect the user back to the application after authentication has been completed. If possible, include a local host and port that is used during development, as this allows the authentication to be completed on a local instance as well. An example of the redirect URL's might look like:

.. code-block:: 

  http://localhost:8100/
  https://sdhp.stfc.skao.int/dp-naledi/dashboard/
  https://sdhp.stfc.skao.int/integration-ska-sdp-dataproduct-dashboard/dashboard/
  https://k8s.stfc.skao.int/staging-ska-sdp-dataproduct-dashboard/dashboard/
  https://sdhp.stfc.skao.int/ci-dev-ska-sdp-dataproduct-dashboard/dashboard/


- Also note that there are two different app registration types, and the one required for your application need to be specified. The first is for front-end SPA (Single Page Applications), the other is for API services for example FastAPI, that requires a WEB API registration. Indicate that in the purpose of the SSO field on the IT support request, as the wrong type of registration will cause the authentication to fail.
- You will be supplied the application registration details as a One Time Secret when the application has been registered. You can save these in the `HashiCorp Vault <https://developer.skao.int/en/latest/tools/secret-management.html>`_ 
- The two required keys are the client ID and tenant id, these can be saved for example as:

.. code-block:: 

  REACT_APP_MSENTRA_CLIENT_ID=...
  REACT_APP_MSENTRA_TENANT_ID=...

Note that when working with React and using environment variables, its a convention to prefix your custom environment variables with REACT_APP_*.

**Integration of the MS Entra Provider into the host**

To enable MS Entra authentication in a SPA, you need to wrap your application in the auth provider. 
This can be simply done by wrapping the code that requires authentication in the AuthProvider that is supplied as part of this library
There are three properties that need to be provided for the AuthPRovider, which will be provided upon successful application registration

.. code-block:: 

  <AuthProvider
      MSENTRA_CLIENT_ID={MSENTRA_CLIENT_ID}
      MSENTRA_TENANT_ID={MSENTRA_TENANT_ID}
      MSENTRA_REDIRECT_URI={MSENTRA_REDIRECT_URI}
   >
        Code that requires authentication here
   </AuthProvider>

**Integration of the environment variables**

Your application will need the following environment variables to be available. You can create them in your .env file as empty strings, and add them to the relevant env.ts and constants as required in your project. When deployed via helm, these two ID's will be populated with values from the vault. When testing locally, a local .env file with the app keys can be used. A way to handle this more securely needs to be determined and caution should be taken not to commit these keys to the repository.

.. code-block:: bash

  REACT_APP_MSENTRA_CLIENT_ID=''
  REACT_APP_MSENTRA_TENANT_ID=''
  REACT_APP_MSENTRA_REDIRECT_URI=''

**Integration into the host chart**

In your host application templates, you need to add the annotations for the use of the vault:

.. code-block:: yaml

      annotations:
      {{ if ((.Values.ui.vault).useVault) }}
          vault.hashicorp.com/agent-inject: "true"
          vault.hashicorp.com/agent-inject-status: "update"
          vault.hashicorp.com/agent-inject-secret-config: "{{ .Values.ui.vault.pathToSecretVault }}"
          vault.hashicorp.com/agent-inject-template-config: |
              {{`{{- with secret `}}"{{ .Values.ui.vault.pathToSecretVault }}"{{` -}}`}}
              {{`{{- range $k, $v := .Data.data }}`}}
              {{`export {{ $k }}={{ $v }}`}}
              {{`{{- end }}`}}
              {{`{{- end }}`}}
          vault.hashicorp.com/role: "kube-role"
      {{ end }}

As well as the environment variables:

.. code-block:: yaml

          env:
            - name: REACT_APP_MSENTRA_REDIRECT_URI
              value: "{{ .Values.ingress.hostname }}{{ include "ska-login-page.ui.ingress.path" . }}/"
            - name: REACT_APP_MSENTRA_CLIENT_ID
              value: "{{ .Values.ui.vault.client_id }}"
            - name: REACT_APP_MSENTRA_TENANT_ID
              value: "{{ .Values.ui.vault.tenant_id }}"


Lastly you need to add the source to your secrets that get injected by the vault into your pod. This can be done by adding the source into your nginx_env_config.sh in the case of a front-end application:

.. code-block:: bash

  source /vault/secrets/config
