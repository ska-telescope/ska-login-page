Integration into a host application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To integrate authentication into a SPA (Single Page Application) there are two options available, both of which have been implemented in the codebase of the 
ska-react-webapp-skeleton.  See https://developer.skao.int/projects/ska-react-webapp-skeleton/en/latest/?badge=latest.

In addition the provided AuthProvider needs to be used to wrap around the application requiring authentication.

**1. Use the AuthWrapper component**

This has been written to provide all the standard functionality of an application:

SKA header
  - SKA Logo
  - Application title placeholder
  - Login button / user button
  - Telescope selector ( optional )
  - Documentation Icon ( optional )
  - Theme Icon
  - Help Icon ( optional )
  - Optional Children placeholder

SKA Footer
  - SKA Copyright + popup
  - Application version placeholder
  - Optional Children placeholder

Use of the AuthWrapper sets up all the standard aspects of an SKA application, allowing the developer to focus on the application specifics

**2. Use of the Individual components**

The components that are needed to implement authentication have been provided individually so that a custom application set up can be created
should there be a need.  

Components:
  - Login Button
  - Logout Button
  - User Button  ( onClick property is available for specific application needs )
  - User Button Menu ( When clicked, a small menu is displayed below the button containing the Logout option ) 

.. tip:: 

   - A simple example of the use of these components in a custom implementation has been provided as part of the `ska-react-webapp-skeleton`