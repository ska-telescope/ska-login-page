Welcome to ska-login-page documentation!
========================================

This is a JavaScript library contained low level components and utilities for use within SKAO GUI applications,
and provide AAA functionality.

It makes use of the following:

Material-UI ( version 5 ) - Ensures that the SKAO theme is implemented in a consistent manner

`ska-gui-components` - JavaScript library containing basic components which have been enhanced in this library

Every effort has been made to ensure that all components have a unique means of identification for testing purposes,
as well as implementation of standard properties to allow for maximum accessibility for those that have access limitations

.. toctree::
  :maxdepth: 1
  :caption: User Guide

  userguide/Overview

.. toctree::
  :maxdepth: 1
  :caption: Developer Guide

  developerguide/Development
  developerguide/Prerequisites
  developerguide/Integration
  developerguide/AxiosFetch

.. toctree::
  :maxdepth: 1
  :caption: Components

  componentguide/authProvider
  componentguide/authWrapper
  componentguide/buttonLogin
  componentguide/buttonLogout
  componentguide/buttonUser
  componentguide/buttonUserMenu
  componentguide/useAPIClient
  componentguide/useUserRoles
  componentguide/version

.. toctree::
  :maxdepth: 1
  :caption: Releases

  CHANGELOG.md
