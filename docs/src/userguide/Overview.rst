Overview
~~~~~~~~

This is a JavaScript library that contains low level components and utilities for use within SKAO GUI applications, and provide AAA functionality.

Usage
=====

The users can authenticate with their user account registered on the SKAO MS Entra instance.

**SKAO MS Entra Authentication**

The following prerequisites need to be in place for the MS Entra authentication to work:

  - The application where the user wants to sign in to need to be deployed with the needed prerequisites in place as detailed in this document's deployment guide.
  - The users need to have an account on the SKAO MS Entra instance.

When making use of MS Entra authentication, the user can sign in by clicking on the sign in button. This will redirect the user to the Microsoft sign in page, where the user can enter the account name they want to use to sign in with:

.. figure:: /_static/img/msEntraSignIn.png
   :width: 60%

   MS Entra sign in page.

